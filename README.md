# ics-ans-role-recceiver

Ansible role to install recceiver.

## Requirements

- ansible >= 2.4
- molecule >= 2.6

## Role Variables

```yaml
recceiver_network: recceiver-network
recceiver_container_name: recceiver
recceiver_image_name: registry.esss.lu.se/ics-software/recsync
recceiver_image_tag: latest
recceiver_image_pull: true
recceiver_image: "{{ recceiver_image_name }}:{{ recceiver_image_tag }}"
recceiver_env: {}
```

## Example Playbook

```yaml
- hosts: servers
  roles:
    - role: ics-ans-role-recceiver
```

## License

BSD 2-clause
